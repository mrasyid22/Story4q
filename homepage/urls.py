from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('education/', views.education, name='education'),
    path('gallery/', views.gallery, name='gallery'),
    path('contact/', views.contact, name= 'contact'),
]