from django.shortcuts import render

def home(request):
    return render(request, 'Home.html')

def about(request):
    return render(request, 'About.html')

def education(request):
    return render(request, 'Education.html')

def gallery(request):
    return render(request, 'gallery.html')

def contact(request):
    return render(request, 'contact.html')
